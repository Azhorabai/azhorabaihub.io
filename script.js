//typewriter
//forked from Gregory Schier

let TxtRotate = function(element, toRotate, period){
    this.toRotate = toRotate;
    this.element = element;
    this.loopCount = 0;
    this.period = parseInt(period, 10) || 800;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtRotate.prototype.tick = function(){
    let i = this.loopCount % this.toRotate.length;
    let completeTxt = this.toRotate[i];

    if(this.isDeleting){
        this.txt = completeTxt.substring(0, this.txt.length - 1);
    }else{
        this.txt = completeTxt.substring(0, this.txt.length + 1);
    };

    this.element.innerHTML = '<span class="wrap">'+this.txt+'</span>';

    let that = this;
    let beta = 300 -Math.random() * 100;

    if(this.isDeleting){beta /= 2; }

    if(!this.isDeleting && this.txt === completeTxt){
        beta = this.period;
        this.isDeleting = true;
    }else if(this.isDeleting && this.txt === ''){
        this.isDeleting = false;
        this.loopCount++;
        beta = 500;
    };

setTimeout(function() {that.tick();}, beta);
};

window.onload = function(){
    let elements = document.getElementsByClassName('txt-rotate');

    for(let i = 0; i < elements.length; i++){
        let toRotate = elements[i].getAttribute('data-rotate');
        let period = elements[i].getAttribute('data-period');

        if(toRotate){
            new TxtRotate(elements[i], JSON.parse(toRotate), period);
        }
    }

    //injecting CSS
    let css = document.createElement("style");
    css.type = "text/css";
    css.innerHtml = ".txt-rotate > .wrap {border-right: 0.08em solid #000000}";
    document.body.appendChild(css);
};

$(function(){
    $("#javascript").fadeIn('slow');
});

//fade-in scroll forked from  Anna Larson
$(document).ready(function(){

    //when windows is scrolled...
    $(window).scroll(function(){

        //checks locations of element...
        $('.skill').each(function(i){
            let element = $(this).position().top + $(this).outerHeight();
            let bottomOfWindow = $(window).scrollTop() + $(window).height();

            //fades in element if fullly visible.
            if(bottomOfWindow > element){
                $(this).animate({'opacity': '1'}, 1500);
            };
        });

    });

});

//slideshow
$(".img-container > div:gt(0)").hide();

setInterval(function(){
    $('.img-container > div:first')
    .fadeOut(1200)
    .next()
    .fadeIn(700)
    .end()
    .appendTo('.img-container');
}, 2500);

//particle effect
particlesJS.load('particles-js', 'particles.json', function(){
    console.log('particles.json loaded');
})
